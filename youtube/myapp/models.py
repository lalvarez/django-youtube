from django.db import models

class Video(models.Model):
    titulo = models.CharField(max_length=64)
    link = models.CharField(max_length=64)
    selected=  models.BooleanField(default=False)
    id= models.CharField(max_length=64, primary_key=True)
    def __str__(self):
        return str(self.id)
