from django.http import HttpResponse
from django.views.decorators.csrf import ensure_csrf_cookie
from django.middleware.csrf import get_token
from .models import Video
from .ytchannel import YTChannel
from django.template import loader
from django.shortcuts import render
# Create your views here.
def main(request):

    if request.method == 'POST':
        if 'id' in request.POST:
            if request.POST.get('select'):
                id1=request.POST['id']
                video = Video.objects.get(id=id1)
                video.selected=True
                video.save()
            elif request.POST.get('deselect'):
                id1=request.POST['id']
                video = Video.objects.get(id=id1)
                video.selected=False
                video.save()
    csrf_token = get_token(request)
    template = loader.get_template('myapp/page.html')
    videos = Video.objects.all()

    context = {
            'named': 'deselect',
            'names': 'select',
            'token': csrf_token,
            'selectedvideos': Video.objects.filter(selected=True),
            'selectablevideos': Video.objects.filter(selected=False),

        }
    return render(request, 'myapp/page.html', context)
