import urllib.request

from django.apps import AppConfig
from .ytchannel import YTChannel

class MyAppConfig(AppConfig):
    name = 'myapp'

    def ready(self):
        from .models import Video

        global videos

        url = 'https://www.youtube.com/feeds/videos.xml?channel_id=' \
            + 'UC300utwSVAYOoRLEqmsprfg'
        xmlStream = urllib.request.urlopen(url)
        channel = YTChannel(xmlStream)

        for video in channel.videos():
            bim = Video(titulo = video['title'], link =  video['link'], selected =  video['selected'], id =  video['id'])
            bim.save()
